<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Anotações YAN</title>
	<style type="text/css">
		body{
			font-family: Comic Sans MS;
		}
	</style>
</head>
<body>
	<form method="get" action="valor.php">
		<br/><br/><br/>
		<fieldset>
			Nome:	<input type="text" name="inNome"/>
			Ano Nascimento: <input type="text" name="inNascimento"/>
			Nota da primeira prova: <input type="number" name="inProva1" min="0" max="10">
			Nota da segunda prova: <input type="number" name="inProva2" min="0" max="10">
			<br/><br/>
			Tamanho da fonte: <input type="range" name="inTamanho" max="25" min="10" value="15">
			cor da fonte: <input type="color" name="inCor" value="#0000FF">
			Qual seu time de coração? <input type="text" name="inTime" list="times">
			<datalist id="times">
				<option>São Paulo</option>
				<option>Palmeiras</option>
				<option>Santos</option>
				<option>Bahia</option>
				<option>Fluminense</option>
			</datalist>
			<br/><br/>
			Informe o número para ser calculado o fatorial: <input type="number" name="inFatorial" min="1" max="15">
			Informe o número para descobrir se é primo: <input type="number" name="inPrimo" min="1" max="300">
			<br/><br/><br/>
			SUPER SOMADOR: <input type="text" name="inNumeros" placeholder="Numeros separados por vírgula. Ex: 4,6,45,7,1,58,498,1,4" size="50">
			<input type="submit" name="botao"/>
		</fieldset>
	</form>

	<?php 
	


	$n1 = 8;
	$n2 = 4;
	

	$negativo = -13;
	echo "<br/><br/><br/><br/><br/><br/>";
	echo "Função que faz o numero ficar positivo, resultado: " . abs($negativo);
	echo "<br/> o valor de $n1 <sup>$n2</sup> é " . pow($n1, $n2);
	echo "<br/> A raiz quadrada de $n1 é " . sqrt($n1);
	$quebrado = 4.54976145;
	echo "<br/>$quebrado";
	echo "<br/> Arredondamentos: $quebrado -> ".round($quebrado)." ou " . ceil($quebrado)." ou " .floor($quebrado);
	echo "<br/> Tuncar o valor de cima: ".intval($quebrado);
	echo "<br/> number format: " . number_format($quebrado,2) . " ou " . number_format($quebrado,4);
	$grande = 9056454;
	echo "<br/> Numero grande formatado: ".number_format($grande,2,",",".");

	// EX 01, USO DE OPERADORES:
	
	echo "<br/>um dos valores digitados foi $n1";
	$n1 *= 1.1;
	echo " esse valor + 10% é:".$n1;

	// cuidado, por exemplo, com o pós-decremento x pré-decremento!!!
	// cuidado, por exemplo, com o pós-incremento x pré-incremento!!!
	$int = 10;
	echo "<br/>int = $int <br/> int++ = " .$int++ ." <br/> ++Sint =". ++$int;

	// Variàvel que faz referência à outra. Se A faz referencia a B, além do valor de B ser igual ao de A, assim que eu alterar o valor de A, o vaor de B também será alterado!  ex:

	$a = 10;
	$b = &$a;
	echo "<br/>a: $a --> b: $b";
	$a = 999;
	echo "<br/>a: $a --> b: $b";

	
	// na hora de criar uma função, se adicionarmos um "&" antes da variável que vai como parâmetro, toda alteração que for feita nessa variável será permanente!  EXEMPLO:

	$inicial = 5;

	function blablabla(&$x){
		$x = $x * 2;
		return $x;
	}
	echo "<br/><br/><br/>";
	echo blablabla($inicial)." agora a variavel 'inicial' vale $inicial";



	
	?>
</body>
</html>